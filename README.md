A python version using the 'python-efl' module to get easy access to the EFL libraries.
'python-efl' is not installed by default on Elive so you will need to do this so as to have this working.

<img src="https://forum.elivelinux.org/uploads/default/optimized/2X/2/204c4c9fba73e86e6df72ebef2d372570006d9c5_2_444x500.jpeg" alt="Elive_Screenshot_2022-04-16_19_41_37__336x444" style="zoom:50%;" />

It might be useful as a template for future pop-ups, considering that the os.system definitions are easily edited and on top, Elementary has a lot more GUI options than 'zenity'.
The script itself is ~~only partially functional~~ primarily meant as a proof of concept.

With a great big 'Thank You' to Jeff Hoogland cs. at BodhiLinux for the excellent tutorials and documentation.
