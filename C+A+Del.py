#!/usr/bin/python3
# encoding: utf-8

import gettext
import os.path
import subprocess
# set translateable text inside _()
_ = gettext.gettext

import efl.elementary as elm
from efl.elementary.window import StandardWindow
from efl.elementary.button import Button
from efl.elementary.box import Box
from efl.elementary.icon import Icon

from efl.evas import EVAS_HINT_EXPAND, EVAS_HINT_FILL, EVAS_ASPECT_CONTROL_VERTICAL, EVAS_ASPECT_CONTROL_BOTH
EXPAND_BOTH = EVAS_HINT_EXPAND, EVAS_HINT_EXPAND
FILL_BOTH = EVAS_HINT_FILL, EVAS_HINT_FILL

# set the path to the icons in the local folder

img_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images")

# point to each icon
ic_file = os.path.join(img_path, "elive-logo.svg")
ic1_file = os.path.join(img_path, "system-log-out.svg")
ic2_file = os.path.join(img_path, "user-home.svg")
ic3_file = os.path.join(img_path, "computer-symbolic.svg")
ic4_file = os.path.join(img_path, "system-reboot.svg")
ic5_file = os.path.join(img_path, "screen-resize.svg")
ic7_file = os.path.join(img_path, "system-users.svg")
ic8_file = os.path.join(img_path, "emblem-important-symbolic.svg")

class MainWindow(StandardWindow):
    def __init__(self):
        win = StandardWindow.__init__(self, "C+A+Del", _("Options:"), size=(300,400), focus_highlight_enabled=True)
        self.callback_delete_request_add(lambda o: elm.exit())
# Set the top logo icon

        ic = Icon(self, file=ic_file, resizable=(False, False))
        ic.show()


        ic1 = Icon(self, file=ic1_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_VERTICAL, 0, 0))
        MyButton = Button(self)
        MyButton.size_hint_weight = (0, 1)
        MyButton.size_hint_align = FILL_BOTH
        MyButton.text = (_("Leave Desktop"))
        MyButton.callback_clicked_add(clicked_cb)
        MyButton.content = (ic1)
        MyButton.show()

        ic2 = Icon(self, file=ic2_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_BOTH, 0, 0))
        MyButton2 = Button(self)
        MyButton2.size_hint_weight = (0, 1)
        MyButton2.size_hint_align = FILL_BOTH
        MyButton2.text = _("Switch User")
        MyButton2.callback_clicked_add(clicked_cb2)
        MyButton2.content = (ic2)
        MyButton2.show()

        ic3 = Icon(self, file=ic3_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_VERTICAL, 0, 0))
        MyButton3 = Button(self)
        MyButton3.size_hint_weight = (0, 1)
        MyButton3.size_hint_align = FILL_BOTH
        MyButton3.text = _("Task Manager")
        MyButton3.callback_clicked_add(clicked_cb3)
        MyButton3.content = (ic3)
        MyButton3.show()

        ic4 = Icon(self, file=ic4_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_VERTICAL, 0, 0))
        MyButton4 = Button(self)
        MyButton4.size_hint_weight = (0, 1)
        MyButton4.size_hint_align = FILL_BOTH
        MyButton4.text = _("Reset Desktop Settings")
        MyButton4.callback_clicked_add(clicked_cb4)
        MyButton4.content = (ic4)
        MyButton4.show()

        ic5 = Icon(self, file=ic5_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_VERTICAL, 0, 0))
        MyButton5 = Button(self)
        MyButton5.size_hint_weight = (0, 1)
        MyButton5.size_hint_align = FILL_BOTH
        MyButton5.text = _("Resize Screen")
        MyButton5.callback_clicked_add(clicked_cb5)
        MyButton5.content = (ic5)
        MyButton5.show()

        ic7 = Icon(self, file=ic7_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_VERTICAL, 0, 0))
        MyButton7 = Button(self)
        MyButton7.size_hint_weight = (0, 1)
        MyButton7.size_hint_align = FILL_BOTH
        MyButton7.text = _("User Configuration Manager")
        MyButton7.callback_clicked_add(clicked_cb7)
        MyButton7.content = (ic7)
        MyButton7.show()

        ic8 = Icon(self, file=ic8_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_VERTICAL, 0, 0))
        MyButton8 = Button(self)
        MyButton8.size_hint_weight = (0, 1)
        MyButton8.size_hint_align = FILL_BOTH
        MyButton8.text = _("Report an Issue to Elive")
        MyButton8.callback_clicked_add(clicked_cb8)
        MyButton8.content = (ic)
        MyButton8.show()

        ic8 = Icon(self, file=ic8_file, size_hint_aspect=(EVAS_ASPECT_CONTROL_BOTH, 0, 0))
        MyButton9 = Button(self)
        MyButton9.size_hint_weight = (0, 1)
        MyButton9.size_hint_align = FILL_BOTH
        MyButton9.text = _("Cancel")
        MyButton9.callback_clicked_add(clicked_cb9)
        MyButton9.content = (ic8)
        MyButton9.show()

        MyBox = Box(self)
        MyBox.size_hint_weight = EXPAND_BOTH
#        MyBox.pack_end(ic)
        MyBox.pack_end(MyButton8)
        MyBox.pack_end(MyButton2)
        MyBox.pack_end(MyButton3)
        MyBox.pack_end(MyButton4)
        MyBox.pack_end(MyButton5)
        MyBox.pack_end(MyButton)
        MyBox.pack_end(MyButton7)
        MyBox.pack_end(MyButton9)
        MyBox.show()

        self.resize_object_add(MyBox)
def clicked_cb(btn):
    subprocess.Popen(["elive-pm", "'logoutmenu'"])
def clicked_cb2(btn):
    subprocess.Popen(["dm-tool", "switch-to-greeter"])
def clicked_cb3(btn):
    subprocess.Popen(["gnome-system-monitor", "-p"])
def clicked_cb4(btn):
    subprocess.Popen(["e17-restart-and-remove-conf-file-WARNING-dont-complain", "--ask"])
def clicked_cb5(btn):
    subprocess.Popen(["/usr/bin/arandr"])
def clicked_cb7(btn):
    subprocess.Popen(["gksu", "user-manager"])
def clicked_cb8(btn):
    subprocess.Popen(["elive-bug-report"])
def clicked_cb9(btn):
    elm.exit()

if __name__ == "__main__":
    elm.init()
    GUI = MainWindow()
    GUI.show()
    elm.run()
    elm.shutdown()
